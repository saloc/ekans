# Changelog
This is the changelog file, containing information about all of the changes made in the project trough the years. Each "change" represents a released version of the program, which has the functionallity planned in the previous one.

### `(29.09.2023)` - Basic token parsing
For this release we have few things to talk about

* ĕnkas now works and `accepts arguments trough the compiler`.
* ĕnkas could not compile non `.eks` files. It `throws an exception`.
* Basic `file structure`.
* ĕnkas now compiles the provided file to a `token` type, which could be used later for parsing and saving into the memory

Example usage of token parsing:
```zsh
(base) ➜  ekans git:(master) ✗ ./enkas util/main.eks

{ i32$a=5; i16$b=69; ui64$$test$=1000; }
{ i8$gg=55; i16$just$$atest=3; s$test=2; }
```

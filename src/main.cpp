#include "enkas.h"

// TODO: Token parsing
void parseToken(std::string token) {
    //[i32; "test", "po"]

    // Step 1: Create the object holder
    // Step 2: Define the name
    // Step 3: Define the operation name
    // Step 4: Define the operation code

    std::istringstream parser(token);

    // Holders
    std::string type;
    std::string name;
    int value;
    short code;

    // readSingleToken(type, "$", parser);

}

int main(int argc, char **argv) {

    if (argc == 1) {
        printUsage();
    } else if (argc == 2) {
        try {
            std::ifstream file = readEksFile(argv[1]);

            std::string line;
            std::string out;
            bool isBracketClosed = true;

            while (getline(file, line)) {
                
                // TODO: Sometimes it inserts way too much &s
                for (std::string & type : datatypes)
                    insertDatatypeSplitter(line, type, "$");

                for (size_t iterator = 0; iterator < line.size(); iterator++) {
                    if (line[iterator] == '\t')
                        continue;
                    if (line[iterator] != ' ')
                        out += line[iterator];

                    if (line[iterator] == '}') {
                        out += '\n';
                        isBracketClosed = true;
                    }

                    if (line[iterator] == '{')
                        isBracketClosed = false;
                        
                }

                if (!isBracketClosed)
                    out += " ";
            }

            std::cout << out;

            file.close();
        } catch (const std::invalid_argument& exc) {
            std::cerr << COMPILER_NAME << ": " << exc.what() << std::endl;
            return -1;
        } catch (const std::runtime_error& exc) {
            std::cerr << COMPILER_NAME << ": " << exc.what() << std::endl;
            return -1;
        }
    }

    return 0;
}
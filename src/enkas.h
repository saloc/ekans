#ifndef ENKAS
#define ENKAS

#include <stdexcept>
#include <sstream>
#include <fstream>
#include <map>

#include "util/exceptions.h"
#include "util/datatypes_parse.h"
#include "util/console_formatting_and_printing.h"
#include "util/file_handling.h"

#endif
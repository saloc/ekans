#ifndef FILE_HANDLING_1320957532895678923
#define FILE_HANDLING_1320957532895678923

#include "../enkas.h"

std::ifstream readEksFile(std::string pathToFile) {

    if (pathToFile.substr(pathToFile.size() - 3, 3) != "eks") {
        throw std::invalid_argument(INVALID_FILENAME_EXCEPTION);
    }

    std::ifstream file(pathToFile);

    return file;
}

void insertDatatypeSplitter(std::string & line, const std::string & searching, const std::string & splitter) {
    size_t var_pos = line.find(searching);
    if (var_pos != std::string::npos) {
        line.insert(var_pos + searching.size() + 1, splitter);
        return;
    }
}

void readSingleToken(std::string & str, std::string splitter, std::istringstream parser) {
    std::string holder;
    while (parser >> holder)
        if (holder == splitter)
            break; // The type definition is done
        else {
            str += holder;
            holder = "";
        }
}

#endif
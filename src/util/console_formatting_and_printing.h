#ifndef CONSOLE_FORMATTING_AND_PRINTING_750556643531408665202
#define CONSOLE_FORMATTING_AND_PRINTING_750556643531408665202

#include <iostream>

void printUsage() {
    std::cout << "\n-- The ENKAS compiler --" << std::endl;
    std::cout << "Usage: ekans program [arguments]" << std::endl; 
    std::cout << "\t -o [name]\t\t" << "output the compiled file with name [name]" << std::endl;
}

#endif // !CONSOLE_FORMATTING_AND_PRINTING_750556643531408665202
#ifndef EXCEPTIONS_602467337164348644157
#define EXCEPTIONS_602467337164348644157

// Titles
#define COMPILER_NAME "ENKAS"


// Exceptions
#define INVALID_FILENAME_EXCEPTION "The provided file format is invalid"
#define EMPTY_DOCUMENT_EXCEPTIONS "The provided file is empty"
#define NO_ELEMENTS_IN_THE_STACK_EXCEPTION "There are no elements in the stack you provided"

#endif // !EXCEPTIONS_602467337164348644157
#include <string>

std::string datatypes[] = {
    "ui8",
    "ui16",
    "ui32",
    "ui64",
    "ui128",
    "i8",
    "i16",
    "i32",
    "i64",
    "i128",
    "us",
    "s",
    "ul",
    "l",
    "f16",
    "f32",
    "d"
};
#ifndef DATA_TYPES_94948077584144320
#define DATA_TYPES_94948077584144320

#include <cctype>

// Unsigned Integer types
typedef __uint8_t   ui8;
typedef __uint16_t  ui16;
typedef __uint32_t  ui32;
typedef __uint64_t  ui64;
typedef __uint128_t ui128;

// Signed Integer types
typedef __int8_t   i8;
typedef __int16_t  i16;
typedef __int32_t  i32;
typedef __int64_t  i64;
typedef __int128_t i128;

// Shorts
typedef __u_short  us;
typedef short       s;

// Longs
typedef __u_long   ul;
typedef long        l;

// Floats
typedef _Float16    f16;
typedef float       f32;
typedef double      d;

#endif
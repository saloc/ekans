#include <iostream>
#include <sstream>

#include "dtypes.h"

#define VAR std::pair<std::string, std::string>

class Variable {

public:

//const VAR & returnVariable(std::string line) { return createVariable(); }

private:
    std::string line;

    VAR createVariable(std::istringstream line);

};

VAR Variable::createVariable(std::istringstream line) {

    VAR current;

    line >> current.first >> current.second; 

    return current;
}
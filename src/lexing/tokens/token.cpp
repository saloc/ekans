#include <iostream>
#include "../dtypes.h"

// Use templates and inheritance!!
// [i32, "test", 0] <Holds the class by itself>
//  ^ -----------
// Could be used as a value?

class Token {

private:
    std::string name;
    short operation;

public:

    const std::string & getName() { return this->name; }
    const short & getOperation() { return this->operation; }

    void setName(std::string name_) { this->name = name_; }
    void setOperation(short operation_) { this->operation = operation_; }

};
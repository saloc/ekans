#include <stack>

class Stack {

    public:
        bool pushIntoStack(int * toPush);
        bool dumpStack();
        int * popFromStack(int timesToPop);
        int * popFromStack();
        int * peekTheTopElement();

    private:
        // TODO: Think about the data types
        std::stack<int *> stack;

        void clearStack(std::stack<int *>& stack_) {
            if (stack_.size() == 0) { return; }

            while (stack_.size() != 0) {
                stack.pop();
            }
        }

};
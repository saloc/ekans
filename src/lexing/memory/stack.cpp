#include <stdexcept>

#include "stack.h"
// #include "src/util/exceptions.h"

#define NO_ELEMENTS_IN_THE_STACK_EXCEPTION "There are no elements in the stack you provided"

// TODO: Check for empty stacks!!
// TODO: Throw exceptions

// Put the element on top of the stack and check if it's containing it
bool Stack::pushIntoStack(int * toPush) {
    this->stack.push(toPush);
    return this->stack.top() == toPush;
}

// Clear the stack and check if the size is 0
bool Stack::dumpStack() {
    clearStack(this->stack);
    return this->stack.size() == 0;
}

// Method which pops few elements from the stack and returns the top one
int * Stack::popFromStack(int timesToPop) {
    int size = this->stack.size();

    if (size == 0) {  
        throw std::invalid_argument(NO_ELEMENTS_IN_THE_STACK_EXCEPTION);
    }

    if (timesToPop > size)
        timesToPop = size;

    int popped = 0;
    int * topElementPointer = this->stack.top();

    while (this->stack.size() > 0 && popped < timesToPop) {
        this->stack.pop();
        topElementPointer = this->stack.top();
    }
    
    return topElementPointer;
}

// Method, which pops one element, and returns the top one from the stack
int * Stack::popFromStack() {
    if (this->stack.size() == 0) {  
        throw std::invalid_argument(NO_ELEMENTS_IN_THE_STACK_EXCEPTION);
    }

    this->stack.pop();
    return this->stack.top();
}

// Method, that just returns the top element from the stack
int * Stack::peekTheTopElement() {
    if (this->stack.size() == 0) {  
        throw std::invalid_argument(NO_ELEMENTS_IN_THE_STACK_EXCEPTION);
    }

    return this->stack.top();
}
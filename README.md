# The ĕnkas programming language
`ĕnkas` is a programming language made by 17 year old with the main goal to create more flexible, yet simple language than our beloved C++. Do not expect much from it, it's made purely just for fun.

### How to run the project
If you want to test the project at the current state you should compile it from source. Here is a simple guide of how to do it.

1. Clone the git repository in a folder of your choice `git clone https://gitlab.com/saloc/ekans`
2. Navigate to the `/src` directory
3. Compile the main.cpp file using a compiler of your choice `g++ main.cpp -o enkas`
4. Move the compiled file into a directory of your choice `mv enkas ../`
5. You could test the program with the provided `main.eks` file, which is located at the `/util` folder `./enkas main.eks`
6. Feel free to play around and mess with the project. If you find an issue, [`report it`](https://gitlab.com/saloc/ekans/-/issues/new)

### Contributing
To contribute to this project, check the [`CONTRIBUTING.md`](/CONTRIBUTING.md) file. There you could find more information about how to fork the repo, how to commit, how to create push requests and which contributions will be accepted and which not. There is a big chance that your idea might be implemented already, so before getting on to work first check the open PR's :)

### License
The project is under MIT license. For more information check the [`LICENSE`](/LICENSE) file in the root of the directory

### Media
 * [`Official Discord Server`](https://discord.gg/V8YFcJc8ye)

----
> He made a programming language, because he could not play CS:GO on Arch...

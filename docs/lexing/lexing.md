# Lexical Parsing and Memorisation (Part 1 - Data types)

### Datatypes
ĕnkas is a versatile programming language that supports a wide range of data types. It offers high flexibility and even allows you to create custom data types using the `dtypedef` keyword. Comments in ĕnkas are denoted by the `#` symbol, simplifying the parsing process.

```
# Example usage of dtypedef
dtypedef i32 int
```

If you need to deallocate memory, you can use the `del` keyword, which effectively releases the allocated memory from the heap.

```
# Example usage of del
dtypedef i32 int;
int age = 5l;

del age;
```

### Datatypes Table
| Data Type   | Description                   |
|-------------|-------------------------------|
| `ui8`       | Unsigned Int (8 bit)          |
| `ui16`      | Unsigned Int (16 bit)         |
| `ui32`      | Unsigned Int (32 bit)         |
| `ui64`      | Unsigned Int (64 bit)         |
| `ui128`     | Unsigned Int (128 bit)        |
| `i8`        | Int (8 bit)                   |
| `i16`       | Int (16 bit)                  |
| `i32`       | Int (32 bit)                  |
| `i64`       | Int (64 bit)                  |
| `i128`      | Int (128 bit)                 |
| `us`        | Unsigned Short                |
| `s`         | Short                         |
| `ul`        | Unsigned Long                 |
| `l`         | Long                          |
* i32 test; -   [i32; "test", "pu"]
* test = 15; -  [i32; "test", "asn", 15]
* del test; -   [i32; "test", "po"]
* { i32 test = 5; } - { [i32, "test", "pu"], [i32; "test", "asn", 5], [*, "dmp"] };


### Operations
pu -    Push
po -    Pop
asn -   Assighn value
dump -  Dump (free the memory)

### Keywords

* `del` - free the memory 

* `ui8` ->     Unsigned Int (8 bit)
* `ui16` ->    Unsigned Int (16 bit)
* `ui32` ->    Unsigned Int (32 bit)
* `ui64` ->    Unsigned Int (64 bit)
* `ui128` ->   Unsigned Int (128 bit)

* `i8` ->      Int (8 bit)
* `i16` ->     Int (16 bit)
* `i32` ->     Int (32 bit)
* `i64` ->     Int (64 bit)
* `i128` ->    Int (128 bit)

* `us` ->      Unsigned Short
* `s` ->       Short
* `ul` ->      Unsigned Long
* `l` ->       Long
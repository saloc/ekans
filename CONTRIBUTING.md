# Contributing to ĕnkas
Welcome to the ĕnkas programming language project! We're excited that you're interested in contributing. Whether you want to report a bug, suggest an enhancement, or contribute code, please take a moment to review this guide.

## Table of Contents

1. [`Code of Conduct`](#code-of-conduct)
2. [`Reporting Bugs`](#reporting-bugs)
3. [`Requesting Features`](#requesting-features)
4. [`Contributing Code`](#contributing-code)
5. [`Pull Request Guidelines`](#pull-request-guidelines)
6. [`Communication`](#communication)

### Code of Conduct
Please note that this project is governed by the ĕnkas Code of Conduct. By participating, you are expected to uphold this code. Please report any unacceptable behavior to our [`moderation team`](mailto:deyan.sirakov2006@abv.bg).

### Reporting Bugs
If you encounter a bug or unexpected behavior in ĕnkas, please help us by [`submitting an issue`](https://gitlab.com/saloc/ekans/-/issues/new) on our GitLab repository. When reporting bugs, please provide as much information as possible:

* A clear and descriptive title.
* Steps to reproduce the bug.
* Expected behavior vs. actual behavior.
* Your operating system and ĕnkas version.

### Requesting Features
We welcome feature requests! If you have an idea for improving ĕnkas or adding new functionality, please [`create an issue`](https://gitlab.com/saloc/ekans/-/issues/new) on our GitLab repository. Clearly describe the feature, its use case, and any potential benefits.

### Contributing Code
We encourage contributions from the community. To contribute code to ĕnkas, follow these steps:

1. [`Fork`](https://gitlab.com/saloc/ekans/-/forks/new) the ĕnkas repository to your GitLab / GitHub account.
2. Clone your fork to your local development environment.
3. Create a new branch for your changes: `git checkout -b my-feature-branch`
4. Make your changes and ensure that they adhere to our coding standards.
5. Write tests if applicable and ensure they pass.
6. Commit your changes with clear and concise commit messages.
7. Push your branch to your fork: `git push origin my-feature-branch`
8. Open a pull request on the ĕnkas repository, describing your changes in detail.

### Pull Request Guidelines
To have your pull request considered for merging, please ensure it meets the following guidelines:

* Your code adheres to our coding standards and conventions.
* You have included documentation for any new features or changes.
* You have written tests where relevant and they pass successfully.
* Your commits have clear and concise commit messages.

Our maintainers will review your pull request, provide feedback, and work with you to address any issues.

### Communication
If you have questions, need clarification, or want to discuss your ideas, please join our official ĕnkas [`Discord server`](https://discord.gg/V8YFcJc8ye). Our community is active and eager to help!

> Thank you for contributing to ĕnkas. Together, we can change the era.